package main

import (
	"fmt"
	"os"

	"gitlab.com/byte-orbit/goecom/products/echo"
	"gitlab.com/byte-orbit/goecom/products/gorm"
)

type Main struct {
	HttpServer *echo.Server
	DB         *gorm.DB
}

func NewMain() *Main {
	return &Main{
		HttpServer: echo.NewServer(),
		DB:         gorm.NewDB(),
	}
}

func (m *Main) Run() error {
	productService := gorm.NewProductService()
	m.HttpServer.ProductService = productService
	m.HttpServer.Domain = "localhost:8888"
	if err := m.HttpServer.Open(); err != nil {
		return err
	}
	return nil
}

func (m *Main) Close() error {
	if m.HttpServer != nil {
		if err := m.HttpServer.Close(); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	m := NewMain()
	if err := m.Run(); err != nil {
		m.Close()
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
