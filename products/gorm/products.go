package gorm

import (
	"gitlab.com/byte-orbit/goecom"
)

type ProductService struct {
}

func NewProductService() *ProductService {
	return &ProductService{}
}

func (p *ProductService) FindProductByID(id int) (*goecom.Product, error) {
	dummy := goecom.Product{
		ID:   1234,
		Name: "Dummy Product",
	}
	return &dummy, nil
}

func (p *ProductService) FindProducts(goecom.ProductFilter) ([]*goecom.Product, error) {
	return make([]*goecom.Product, 0), nil
}
