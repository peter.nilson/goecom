package echo

import (
	"github.com/labstack/echo/v4"
)

func (s *Server) registerRoutes(group *echo.Group) {
	group.Add("GET", "/products", s.handleProductGet)
}

func (s *Server) handleProductGet(ctx echo.Context) error {
	product, err := s.ProductService.FindProductByID(1234)
	if err != nil {
	  return err
	}
	if err := ctx.JSON(200, ProductResponse{product}); err != nil {
		return err
	}
	return nil
}

type ProductResponse struct {
	Data interface{} `json:"data"`
}
