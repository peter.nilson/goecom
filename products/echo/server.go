package echo

import (
	"context"

	"github.com/labstack/echo/v4"
	"gitlab.com/byte-orbit/goecom"
	"time"
)

const ShutdownTimeout = 1 * time.Second

type Server struct {
	server *echo.Echo

	Domain string

	ProductService goecom.ProductService
}

func NewServer() *Server {
	s := &Server{
		server: echo.New(),
	}
	productRoute := s.server.Group("")
	productRoute.Use() // Register Middleware here for the routes
	s.registerRoutes(productRoute)
	return s
}

func (s *Server) Open() error {
	if err := s.server.Start(s.Domain); err != nil {
		return err
	}
	return nil
}

func (s *Server) Close() error {
	ctx, cancell := context.WithTimeout(context.Background(), ShutdownTimeout)
	defer cancell()
	return s.server.Shutdown(ctx)
}
