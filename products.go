package goecom

import "time"

type Product struct {
	ID    int           `json:"id"`
	Name  string        `json:"name"`
	Price *ProductPrice `json:"price"`

	CreatedAt time.Time `json:"created_at"`
	EditedAt  time.Time `json:"edited_at"`
}

type ProductPrice struct {
	ID      int
	Amount  float32
	Created time.Time
	Edited  time.Time
}

type ProductService interface {
	FindProducts(ProductFilter) ([]*Product, error)
	FindProductByID(id int) (*Product, error)
}

type ProductFilter struct {
	name string
}
